import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import * as path from 'path'



// import path from 'node:path';
// import { createRequire } from 'node:module';

// // import { defineConfig } from 'vite';
// // import { viteStaticCopy } from 'vite-plugin-static-copy';

// const require = createRequire(import.meta.url);
// const cMapsDir = path.join(path.dirname(require.resolve('pdfjs-dist/package.json')), 'cmaps');
// const standardFontsDir = path.join(
//   path.dirname(require.resolve('pdfjs-dist/package.json')),
//   'standard_fonts',
// );

// import mkcert from 'vite-plugin-mkcert'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: [{ find: '@', replacement: path.resolve(__dirname, 'src') }],
  },
  server: {
    watch: {
      usePolling: true,
    },
    host: true, // needed for the Docker Container port mapping to work
    strictPort: true,
    // https: true
    // port: 3000, // you can replace this port with any port
  }
})
