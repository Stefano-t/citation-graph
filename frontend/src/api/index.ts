import axios from "axios";

const host = "http://localhost:8080"

export async function getFromDOI(type="DOI",id : string, maxDepth = 0) {
    let {data} = await axios.get(`${host}/paper?type=${type}&id=${id}&maxDepth=${maxDepth}`, {
        headers:{
            "Content-Type": "application/json"
        }
    })
    if (typeof data === 'string') {
            return JSON.parse(data)
    }
    else {
        return data
    }
}