export interface TreeNode {
    id?: string
    title?: string
    description?: string
    collapsed?: boolean
    color?: string
    paperId?: string
    abstract?: string
    externalIds?: ExternalIds
    openAccessPdf?: OpenAccessPdf
    authors?: Author[]
    references?: any[]
    childLinks?: any[]
    index?: number
    x?: number
    y?: number
    z?: number
    vx?: number
    vy?: number
    vz?: number
    __threeObj?: ThreeObj
}

export interface ExternalIds {
    DOI: string | undefined
}

export interface OpenAccessPdf {
    url: string | undefined | null
}

export interface Author {
    authorId: string
    name: string
}

export interface ThreeObj {
    metadata: Metadata
    geometries: Geometry[]
    materials: Material[]
    object: Object
}

export interface Metadata {
    version: number
    type: string
    generator: string
}

export interface Geometry {
    uuid: string
    type: string
    radius: number
    widthSegments: number
    heightSegments: number
    phiStart: number
    phiLength: number
    thetaStart: number
    thetaLength: number
}

export interface Material {
    uuid: string
    type: string
    color: number
    emissive: number
    reflectivity: number
    refractionRatio: number
    depthFunc: number
    depthTest: boolean
    depthWrite: boolean
    colorWrite: boolean
    stencilWrite: boolean
    stencilWriteMask: number
    stencilFunc: number
    stencilRef: number
    stencilFuncMask: number
    stencilFail: number
    stencilZFail: number
    stencilZPass: number
}

export interface Object {
    uuid: string
    type: string
    layers: number
    matrix: number[]
    geometry: string
    material: string
}



export interface Paper {
    paperId: string;
    title: string;
    abstract: string;
    externalIds: {
        DOI: string;
        ArXiv: string;
        MAG: string;
        ACL: string;
    }
    openAccessPdf: OpenAccessPdf
    authors: {
        authorId: string;
        name: string;
    }[];
    references: Paper[];
}
export interface Link {
    source: string;
    target: string;
}
