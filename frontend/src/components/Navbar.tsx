import { KeyboardEvent, useState } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom'
import { SearchIcon } from '@heroicons/react/solid'

export default function Navbar() {
    const [searchParams, setSearchParams] = useSearchParams()
    const [depth, setDepth] = useState(searchParams.get("depth"))
    const navigate = useNavigate()
    const changeQuery = (e: KeyboardEvent) => {
        if (e.key === 'Enter' || e.keyCode === 13) {
            let params = new URLSearchParams();
            params.append('paper', e.target.value.replaceAll(".", "@"));
            params.append('depth', depth);
            setSearchParams(params)
        }
    }
    return (
        <div className='h-[60px] flex items-center gap-4 justify-between p-2 bg-white w-full text-red-900 border-b-[#abc0ff] border-b-2'>
            {/* <svg srx></svg> */}
            <div className='h-full flex justify-center items-center gap-2'>

                <img src='/logo.png' className='h-full cursor-pointer' onClick={() => navigate('/')} />
                <div className='text-black italic text-xl'>Citations Graph</div>
            </div>

            <div className='flex gap-4'>
                <select className='bg-white text-black' value={depth} onChange={(e) => setDepth(e.target.value)}>
                    <option value={0}>0</option>
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                </select>
                <input className='bg-white border-[2px] rounded text-black' onKeyUp={(e: KeyboardEvent<HTMLInputElement>) => changeQuery(e)} />
                <SearchIcon className="text-blue-500 w-7 h-7" />
            </div>
        </div>
    )
}
