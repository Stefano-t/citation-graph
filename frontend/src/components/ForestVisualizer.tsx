import { useEffect, useState } from 'react'
import { getFromDOI } from "@/api/index"
import { ForceGraph2D, ForceGraph3D } from 'react-force-graph'
import { Link, Paper, TreeNode } from '@/interfaces/TreeNode';

const colors = ['#ff0000', '#00ff00', '#0000ff', '#ffffff']
import { Dna } from  'react-loader-spinner'

interface ForestVisualizerProps {
    onSelection: Function;
    searchValue: string;
    searchDepth: number;
}

export default function ForestVisualizer({ onSelection, searchValue, searchDepth }: ForestVisualizerProps) {
    const [rootId, setRootId] = useState("4062045");
    const [isLoading, setIsLoading] = useState(false);
    const [mode, setMode] = useState("3d");
    const [prunedTree, setPrunedTree] = useState<{ links: Link[], nodes: TreeNode[] }>({ links: [], nodes: [] });
    const [sliderValue, setSliderValue] = useState(searchDepth);

    const handleSliderChange = (event) => {
      setSliderValue(event.target.value);
    //   fetchData(event.target.value)
    };
    useEffect(() => { fetchData() }, [searchValue, searchDepth])
    const alreadySeen: string[] = [];
    /**
     * The function `citationList` takes a `Paper` object and returns a list of `TreeNode` objects
     * representing the citations of the paper, with each citation having a specified depth.
     * @param {Paper} paper - The "paper" parameter is an object that represents a research paper. It
     * likely has properties such as "paperId", "abstract", and "citations".
     * @param [depth=1] - The `depth` parameter is used to keep track of the depth of the citation tree. It
     * starts with a default value of 1 and is incremented by 1 for each level of citations.
     * @returns The function `citationList` returns an array of `TreeNode` objects.
    */
    const citationList = (paper: Paper, depth = 1) => {
        const papers: TreeNode[] = [];
        paper.references.forEach((p) => {
            if (!alreadySeen.includes(p.paperId)) {
                alreadySeen.push(p.paperId)
                papers.push({ id: p.paperId, collapsed: false, color: colors[depth], ...p })

            }
            papers.push(...citationList(p, depth + 1));
        });
        return papers
    };

    /**
     * The function `linkList` recursively generates a list of links between a paper and its citations.
     * @param {Paper} paper - The parameter "paper" is an object of type "Paper".
     * @returns The function `linkList` returns an array of `Link` objects.
     */
    const linkList = (paper: Paper) => {
        const links: Link[] = [];
        paper.references.forEach((c) => {
            links.push({ source: paper.paperId, target: c.paperId })
            links.push(...linkList(c))
        })
        return links
    }

    
    function isDOIMatch(input:string) {
            const doiRegex = /^10\.\d{4,9}\/[-._;()\/:a-zA-Z0-9]+$/;
            return doiRegex.test(input);
        }
        
    function isArXivMatch(input: string) {
        const arxivRegex = /^arXiv:\d{4,}\.\d{4,}(v\d+)?$/;
        return arxivRegex.test(input);
    }
    /**
     * The `fetchData` function retrieves data from a DOI, processes the data to create a tree structure
     * of nodes and links, and sets the pruned tree as the state.
     */
    const fetchData = async (depth = sliderValue) => {
        let type = "QUERY"
        if (isArXivMatch(searchValue)) {
            type = "ARXIV"
        }
        if (isDOIMatch(searchValue)) {
            type = "DOI"
        }
        setIsLoading(true)
        /* Our testing DOI: 10.3389/frma.2020.607286 */
        const data = await getFromDOI(type, searchValue, depth);
        console.log(data)
        let tmp: { nodes: TreeNode[], links: Link[] } = {};
        if (data.length) {
            tmp.nodes = []
            tmp.links = []
            data.forEach(root => {
                
                tmp.nodes.push(
                           {
                               collapsed: false,
                               id: root.paperId,
                               color: colors[0],
                               description: root.abstract,
                               ...root
                           },
                           ...citationList(root))
                       
                    tmp.links.push(...linkList(root))
                    console.log(tmp)
                   
                   const nodesById = Object.fromEntries(tmp.nodes.map(node => [node.id, node]));
           
                   // link parent/children
                   tmp.nodes.forEach((node: TreeNode) => {
                       node.collapsed = node.id !== rootId;
                       node.childLinks = [];
                   });
                   tmp.links.forEach(link => nodesById[link.source].childLinks.push(link));
                });
                onSelection({
                    collapsed: false,
                    id: data[0].paperId,
                    color: colors[0],
                    description: data[0].abstract,
                    ...data[0]
                })
        } else {
        setRootId(data.paperId)
         tmp = {
            nodes: [
                {
                    collapsed: false,
                    id: data.paperId,
                    color: colors[0],
                    description: data.abstract,
                    ...data
                },
                ...citationList(data)
            ],
            links: linkList(data)
        };
        const nodesById = Object.fromEntries(tmp.nodes.map(node => [node.id, node]));

        // link parent/children
        tmp.nodes.forEach((node: TreeNode) => {
            node.collapsed = node.id !== rootId;
            node.childLinks = [];
        });
        tmp.links.forEach(link => nodesById[link.source].childLinks.push(link));
        onSelection({
            collapsed: false,
            id: data.paperId,
            color: colors[0],
            description: data.abstract,
            ...data
        })
}
console.log(tmp)
        setPrunedTree(tmp);
        setIsLoading(false)

    };
    const getForceGraph = (props) => mode === '3d' ? <ForceGraph3D {...props} /> : <ForceGraph2D {...props} />
    return (
        <div className='relative w-full h-full'>
          {isLoading && <div className='absolute z-20 flex items-center justify-center w-full h-full bg-[#ffffff64]'>  <Dna
    height = "80"
    width = "80"
    wrapperClass='dna-wrapper'
    ariaLabel = 'loading'     
    visible={isLoading}

  /></div>}
            <script src="//unpkg.com/three"></script>
            <div className='absolute z-20 flex justify-between w-full gap-1 px-5 py-1'>
                <div className='flex gap-1'>
                <div onClick={() => setMode('2d')} className={`px-1 text-xs border rounded-md cursor-pointer ${mode === '2d' ? "bg-white text-[#000012]" : ""}`}>2D</div>
                <div onClick={() => setMode('3d')} className={`px-1 text-xs border rounded-md cursor-pointer ${mode === '3d' ? "bg-white text-[#000012]" : ""}`}>3D</div>
                </div>
                <div className='flex items-center justify-center gap-4 text-xs'>Graph Depth: <input
        type="range"
        min="0"
        max={searchDepth}
        value={sliderValue}
        onChange={handleSliderChange}
        onMouseUp={() => fetchData()}
      />{sliderValue}</div>
            </div>
            {getForceGraph(
                {
                    width: window.innerWidth * 2 / 3,
                    height: window.innerHeight - 60,
                    nodeThreeObject: (node: TreeNode) =>
                        //@ts-ignore
                        new THREE.Mesh(new THREE.SphereGeometry(10),
                            //@ts-ignore
                            new THREE.MeshLambertMaterial({
                                color: node.color,
                                transparent: false,
                                opacity: 1
                            })
                        ),
                    graphData:
                        prunedTree,
                    nodeLabel: (node: TreeNode) => ` ${node.title}`,
                    onNodeClick: (node: TreeNode) => onSelection(node),
                    showNavInfo: false,
                    linkWidth: 2,
                    // @ts-ignore
                    linkDistance: 50
                }
            )}

        </div>
    )
}
