import { useImperativeHandle, useState } from 'react'
import PDFVisualizer from '@/components/PDFVisualizer';
import { DocumentIcon, NewspaperIcon, AcademicCapIcon } from '@heroicons/react/outline'
import { TreeNode } from '@/interfaces/TreeNode';

export default function PaperInfo({ paper, parentRef } : {paper: TreeNode, parentRef: any}) {
    
    useImperativeHandle(parentRef, () => ({
        setPdf(value: boolean) { setShowPDF(value) }
    }))

    const [showPDF, setShowPDF] = useState<boolean>(false)

    return (
        <>
            {paper && <> {!showPDF &&
                <div className={`h-full w-full div-animation div-0 bg-white overflow-auto`}>
                    <div className='w-full pt-5 pb-5 pl-5 pr-1 text-black'>

                        <div className='text-lg font-extrabold '>{paper?.title}</div>
                        <div className='my-2 text-[#747070] flex gap-2 flex-wrap'><p className='font-bold'>Authors:</p>{paper?.authors?.map(({ name }, index) => (<p key={index}>{name}{index === paper.authors.length - 1 ? "" : ","} </p>))}</div>
                        <div className='flex items-center gap-5'>
                            {paper.openAccessPdf?.url && <div className='flex items-center text-xs text-black cursor-pointer' onClick={() => { setShowPDF(!showPDF); }}>
                                <DocumentIcon className="text-red-500 w-7 h-7" /> PDF
                            </div>}
                            <div className='flex items-center text-xs text-black cursor-pointer' onClick={() => { window.open(`https://www.semanticscholar.org/paper/${paper.id}`, "_blank") }}>
                                <AcademicCapIcon className="text-red-500 stroke-1 w-7 h-7" /> Semantic Scholar
                            </div>
                        </div>


                        <div className='pr-3 my-2'>{paper?.abstract}</div>
                    </div>
                </div>}
                {<div className={`${showPDF ? "block" : "hidden"} h-full w-full div-animation div-1 relative`}>
                    <div className='absolute z-10 flex items-center  text-xs font-bold rounded bg-[#606165] text-white  cursor-pointer top-5 left-[43%] px-2' onClick={() => { setShowPDF(!showPDF); }}>
                        <NewspaperIcon className="text-white stroke-1 w-7 h-7" /> Paper info
                    </div>
                    <div className={`${showPDF ? 'h-full' : 'h-0'}`}>
                        <PDFVisualizer paper={paper} />
                    </div>
                </div>}

            </>}
        </>
    )
}
