import { pdfjs, Document, Page } from 'react-pdf';
import type { PDFDocumentProxy } from 'pdfjs-dist';
import { useState } from 'react';
import { TreeNode } from '@/interfaces/TreeNode';
import { PlusIcon } from '@heroicons/react/solid'
import { MinusIcon } from '@heroicons/react/solid'
                

pdfjs.GlobalWorkerOptions.workerSrc = new URL(
    'pdfjs-dist/build/pdf.worker.min.js',
    import.meta.url,
).toString();

const options = {
    cMapUrl: '/cmaps/',
    standardFontDataUrl: '/standard_fonts/',
};


export default function PDFVisualizer({ paper }: { paper: TreeNode | null | undefined }) {
    const [numPages, setNumPages] = useState<number>();
    const [scale, setScale] = useState<number>(1);
    function onDocumentLoadSuccess({ numPages: nextNumPages }: PDFDocumentProxy): void {
        setNumPages(nextNumPages);
    }
    return (
        <>
        <div className='absolute z-30 flex right-3 gap-2 top-5 '>

            <div> <PlusIcon className="border bg-gray-500 text-white rounded w-5 h-5 cursor-pointer"  onClick={() => setScale(scale + .1)} /> </div> 
            <div> <MinusIcon className="border bg-gray-500 text-white rounded w-5 h-5 cursor-pointer" onClick={() => setScale(scale - .1)}  /> </div> 
        </div>
            <Document
                className={'!w-full h-full overflow-auto'}
                file={`http://localhost:8080/pdf?PDF=${paper?.openAccessPdf?.url}`}
                onLoadSuccess={onDocumentLoadSuccess}
                options={options}
            >
                {/* <div className='sticky bottom-0 right-0 z-10 text-black'>
                    <div className='w-[10px] h-[10px]'>+</div>
                    <div className='w-[10px] h-[10px]'>-</div>
                </div> */}
                {Array.from(new Array(numPages), (_, index) => (
                    <Page
                        key={`page_${index + 1}`}
                        scale={scale}
                        pageNumber={index + 1}
                    />
                ))}
            </Document>
            <style>{`
            .react-pdf__Page{
            }
            .react-pdf__Page > div{
                display: none
            }
            `}</style>
        </>
    )
}
