// @ts-ignore
import Index from "@/pages/index"
import Home from "@/pages/home"
import Paper from "@/pages/paper"
import {
  Route,
  createBrowserRouter,
  createRoutesFromElements,
} from "react-router-dom";
const router = createBrowserRouter(
  createRoutesFromElements(
    <>


    <Route path="/" element={<Index />}>
      <Route index path="/" element={<Home />}></Route>
      <Route path="/paper" element={<Paper />}></Route>
    </Route>
    </>
  )
);
export default router;