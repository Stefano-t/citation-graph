import React, { useEffect, useState } from 'react'
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import Navbar from "@/components/Navbar"


export default function Index() {
  const location = useLocation()
    return (
      <>
          {location.pathname != '/' && 
          (<Navbar/>)
          }
          <Outlet />
  
      </>
    )
}
