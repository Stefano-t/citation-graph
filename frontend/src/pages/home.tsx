import { useState } from 'react'

import { SearchIcon } from '@heroicons/react/solid'
import { useNavigate } from "react-router-dom";

export default function Home() {
  const [paper, setPaper] = useState("")
  const [depth, setDepth] = useState(1)
  const navigate = useNavigate();

  return (

    <>
      <div className='w-full h-full bg-[#e9e9e9] text-black'>
        <div className='flex flex-col items-center justify-center w-full h-full pb-32'>
          <img src='/logo.png' className='absolute z-0' />
          <div><p className='m-10 text-7xl text-white drop-shadow-[1px_0.2px_10px_rgba(0,0,0,2)] z-1 relative'>Citations Graph</p></div>
          <div className='relative flex items-center justify-center w-full'>
            <div className='absolute left-[calc(75%-50px)] z-10'>
              <SearchIcon className="text-blue-500 w-7 h-7" />
            </div>
            <input

              type='text'
              value={paper}
              onChange={(e) => setPaper(e.target.value)}
              onKeyUp={(e) => {
                if (e.key === 'Enter' || e.keyCode === 13) {
                  let params = new URLSearchParams();
                  params.append('paper', paper.replaceAll(".", "@"));
                  params.append('depth', depth.toString());
              
                  navigate(`/paper?${params}`);

                }
              }}
              className='z-1 relative px-10 w-1/2 h-[60px] bg-white border border-[#d5d1d1] rounded-full'
            />
          </div>
          <div className='w-1/2 mt-2 h-[30px] flex justify-end items-center gap-2'>
            Max Depth: 
            <select className='bg-white' value={depth} onChange={(e) => setDepth(e.target.value)}>
              <option value={0}>0</option>
              <option value={1}>1</option>
              <option value={2}>2</option>
              <option value={3}>3</option>
             </select> 
          </div>
        </div>
      </div>
    </>
  )
}
