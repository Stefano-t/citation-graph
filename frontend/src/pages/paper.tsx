import { useEffect, useRef, useState } from 'react'
import { useParams, useSearchParams } from 'react-router-dom';
/** Types */
import { TreeNode } from '@/interfaces/TreeNode';
/** Components */
import ForestVisualizer from "@/components/ForestVisualizer";
import PaperInfo from '@/components/PaperInfo';



export default function Paper() {
  const [searchParams, setSearchParams] = useSearchParams()
  const ref = useRef();
    const [paper, setPaper] = useState<null | TreeNode>()
    useEffect(() => {console.log("Search : ",searchParams.get("search"))}, [searchParams])
    return (
        <div className='flex w-full h-[calc(100%-60px)]'>
            <div className={`w-1/3 h-full overflow-auto relative `}>
                {paper && <PaperInfo paper={paper} parentRef={ref}/>}
            </div>
            <div className='w-2/3 h-full' id='container-3d'>
                <ForestVisualizer
                    onSelection={(paper: TreeNode) => { console.log(paper); ref.current?.setPdf(false); setPaper(paper) }}
                    searchValue={searchParams.get("paper")?.replaceAll("@", '.') || ""}
                    searchDepth={parseInt(searchParams.get("depth") || 1 ) }
                />
            </div>
            <style>
                {`
    .div-animation {
        animation: slide-out 0.5s forwards;
      }
      
      .div-0 {
        animation: slide-in 0.5s forwards;
      }
      .div-1 {
        animation: slide-in 0.5s forwards;
      }
      
      @keyframes slide-out {
        0% { transform: translateX(0%); }
        100% { transform: translateX(-100%); }
      }
      
      @keyframes slide-in {
        0% { transform: translateX(100%); }
        100% { transform: translateX(0%); }
      }
      
    `}
            </style>
        </div>

    )
}
